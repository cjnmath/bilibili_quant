#! /home/jc/Documents/code/python-virtual-enviroment/scrap/bin/python
import subprocess
import os

script_dir = os.path.dirname(os.path.relpath(__file__))
python = "/home/jc/Documents/code/python-virtual-enviroment/scrap/bin/python"
commands = (
    [python, os.path.join(script_dir, "update_bilibili.py")],
    [python, os.path.join(script_dir, "download.py")],
)

for command in commands:
    subprocess.call(command)
