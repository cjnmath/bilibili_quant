#! /home/jc/Documents/code/python-virtual-enviroment/scrap/bin/python

import sys
from utiles import delete_row_by_mid
from utiles import extract_mid


def main():
    url = sys.argv[1]
    if not url.startswith('https://space.bilibili.com'):
        print("[!!!] Not subscriable url, please check!")
        return
    mid = extract_mid(url)
    delete_row_by_mid(str(mid))
    print("[---] Successfully unsubscribe!")


if __name__ == '__main__':
    main()
