#! /home/jc/Documents/code/python-virtual-enviroment/scrap/bin/python
import subprocess
import setting
from utiles import load_aid_by_download_status
from utiles import update_download_status_by_aid
from utiles import back_up_db
import os

s_dir = setting.STORAGE_DIR
base_url = "https://www.bilibili.com/video/av"


def dowload_by_aid(aid, keep=False):
    main_command = ['/usr/local/bin/you-get', '--playlist', '--output-dir']
    main_command.append(s_dir)

    # main_command = ['youtube-dl', '--yes-playlist', '-o']
    # ytdl_s_dir = os.path.join(s_dir, '%(title)s.%(ext)s')
    # # # print(ytdl_s_dir)
    # main_command.append(ytdl_s_dir)

    main_command.append(base_url+aid)
    print(' '.join(main_command))
    r = subprocess.call(main_command)
    if not keep:
        sub_command = ['find', s_dir, '-maxdepth', '1', '-iname', '*.xml', '-delete']
        subprocess.call(sub_command)
    return r


def main():
    waiting_list = load_aid_by_download_status(download_status=0)
    s = len(waiting_list)
    n = 1
    for aid in waiting_list:
        aid = str(aid)
        r = dowload_by_aid(aid)
        while r != 0:
            r = dowload_by_aid(aid)
        print("[***] Finish downloading extract for {}/{}: {}".format(n, s, base_url+aid))
        n += 1
        update_download_status_by_aid(aid)
    back_up_db()

if __name__ == '__main__':
    main()
