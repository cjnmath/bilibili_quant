
import os
import requests
import json
import re
import sqlite3
import time
import subprocess
from lxml.html import fromstring
from setting import DB_SAVE_DIR


script_dir = os.path.dirname(os.path.relpath(__file__))
# db_dir = os.path.join(script_dir, 'data.db')
db_dir = os.path.join(DB_SAVE_DIR, 'bilibili.data.db')
back_up_dir = os.path.join(DB_SAVE_DIR, '.bilibili.data.backup.db')
HEADERS = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.9 Safari/537.36"
}

MOBILE_HEADERS = {
    "User-Agnet": "Mozilla/5.0 (Linux; Android 7.0; PLUS Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.98 Mobile Safari/537.36"
}


def get_response(url, params=None, headers=HEADERS, proxies=None):
    response = requests.get(url, params=params, headers=headers, proxies=proxies)
    return response


def get_root(url, params=None, headers=HEADERS, encoding='utf-8', proxies=None):
    response = get_response(url, params=params, headers=headers, proxies=proxies)
    response.encoding = encoding
    root = fromstring(response.text)
    return root


def clean_special_char(line):
    """
        dealing with title
    """
    line = re.sub(r'/', '-', line)
    line = re.sub(r'\|', '-', line)
    return line


def extract_aid(url):
    if 'bangumi/play' in url:
        response = get_response(url)
        return int(re.search('AV(\d+)', response.text).group(1))
    return int(re.search('av(\d+)', url).group(1))


def extract_mid(url):
    """
        extract mid from url
    """
    return re.search('space\.bilibili\.com\/(\d+)', url).group(1)


def get_update_docs(mid=165360675):
    url = 'https://space.bilibili.com/ajax/member/getSubmitVideos'
    params = {
            "mid": mid,
            "pagesize": 30,
            "tid": 0,
            "page": 1,
            "order": "pubdate"
    }
    response = requests.get(url, params=params)
    d = json.loads(response.text)
    r = d['data']['vlist']
    try_count = 1
    max_try = 3
    while not r and try_count <= max_try:
        response = requests.get(url, params=params)
        d = json.loads(response.text)
        r = d['data']['vlist']
        try_count += 1
    return r

def check_make_db():
    if not os.path.exists(db_dir):
        open(db_dir, 'w').close()
        if os.path.exists(back_up_dir):
            commad = ['cp', back_up_dir, db_dir]
            subprocess.call(commad)
        else:
            query_1 = """
                CREATE TABLE download_log (aid TEST, title TEST, mid TEXT, author TEXT, download_status BOOLEAN, add_date DATETIME);
            """
            conn = sqlite3.connect(db_dir)
            cursor = conn.cursor()
            cursor.execute(query_1)
            conn.commit()
            conn.close()


def back_up_db():
    command_a = ['touch', back_up_dir]
    command_b = ['cp', db_dir, back_up_dir]
    subprocess.call(command_a)
    subprocess.call(command_b)
    print("[---] Sucessfully backup database!")


def write_row_by_doc(doc, download=0):
    aid = doc['aid']
    title = doc['title']
    mid = doc['mid']
    author = doc['author']
    download_status = download
    add_date = int(time.time())
    t = (aid, title, mid, author, download_status, add_date)
    conn = sqlite3.connect(db_dir)
    cursor = conn.cursor()
    query = """
        INSERT INTO download_log VALUES (?, ?, ?, ?, ?, ?);
    """
    cursor.execute(query, t)
    conn.commit()
    conn.close()


def write_row_by_mid(mid):
    t = (str(mid),)
    conn = sqlite3.connect(db_dir)
    cursor = conn.cursor()
    query = """
        INSERT INTO subscribe_log VALUES (?);
    """
    cursor.execute(query, t)
    conn.commit()
    conn.close()


def write_row_by_aid(aid):
    add_date = int(time.time())
    t = (aid, 'extra', 'extra', 'extra', 0, add_date)
    query = """
        INSERT INTO download_log VALUES (?, ?, ?, ?, ?, ?);
    """
    conn = sqlite3.connect(db_dir)
    cursor = conn.cursor()
    cursor.execute(query, t)
    conn.commit()
    conn.close()


def delete_row_by_aid(aid):
    conn = sqlite3.connect(db_dir)
    cursor = conn.cursor()
    query = """
        DELETE FROM download_log WHERE aid=?
    """
    t = (aid,)
    cursor.execute(query, t)
    conn.commit()
    conn.close()


def delete_row_by_mid(mid):
    conn = sqlite3.connect(db_dir)
    cursor = conn.cursor()
    query_1 = """
        DELETE FROM download_log WHERE mid=?
    """
    t = (mid,)
    query_2 = """
        DELETE FROM subscribe_log WHERE mid=?
    """
    cursor.execute(query_1, t)
    cursor.execute(query_2, t)
    conn.commit()
    conn.close()


def update_download_status_by_aid(aid):
    t = (aid,)
    query = """
        UPDATE download_log SET download_status=1 WHERE aid=?
    """
    conn = sqlite3.connect(db_dir)
    cursor = conn.cursor()
    cursor.execute(query, t)
    conn.commit()
    conn.close()


def load_all_aid():
    results = []
    query = """
        SELECT aid FROM download_log;
    """
    conn = sqlite3.connect(db_dir)
    cursor = conn.cursor()
    cursor.execute(query)
    conn.commit()
    for i in cursor:
        results.append(i[0])
    conn.close()
    return results


def load_aid_by_download_status(download_status=0):
    t = (download_status,)
    query = """
        SELECT aid FROM download_log WHERE download_status=?
    """
    conn = sqlite3.connect(db_dir)
    cursor = conn.cursor()
    cursor.execute(query, t)
    conn.commit()
    results = []
    for i in cursor:
        results.append(i[0])
    conn.close()
    return results


def load_all_mid():
    query = """
        SELECT mid FROM subscribe_log;
    """
    conn = sqlite3.connect(db_dir)
    cursor = conn.cursor()
    cursor.execute(query)
    conn.commit()
    results = []
    for i in cursor:
        if i[0] != 'extra' and i[0]:
            results.append(i[0])
    conn.close()
    return set(results)
