#! /home/jc/Documents/code/python-virtual-enviroment/scrap/bin/python

from utiles import load_all_mid
from utiles import load_aid_by_download_status
from utiles import load_all_aid
from utiles import get_update_docs
from utiles import write_row_by_doc
from download import dowload_by_aid


def main():
    old_mids = load_all_mid()

    # downloaded_aids = load_aid_by_download_status(download_status=1)

    kept_aids = load_all_aid()
    aid_base = "https://www.bilibili.com/video/av"
    for mid in old_mids:
        docs = get_update_docs(mid)
        if docs:
            author = docs[0]['author']
        else:
            print('[!!!] error mid:', mid)
            print('[!!!] Please check https://space.bilibili.com/{}/#/'.format(mid))
            continue
        print("[---] Start updating for {} https://space.bilibili.com/{}/#/".format(author, mid))
        for doc in docs:
            aid = doc['aid']
            if aid not in kept_aids:
                print("[***] find new {} - {}".format(doc['title'], author), aid_base+str(aid))

                r = dowload_by_aid(str(aid))
                while r != 0:
                    r = dowload_by_aid(str(aid))
                write_row_by_doc(doc, download=1)


        print("[---] Finish updating for {}".format(author))


if __name__ == '__main__':
    main()
