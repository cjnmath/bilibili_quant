#! /home/jc/Documents/code/python-virtual-enviroment/scrap/bin/python
import sys
from utiles import extract_mid
from utiles import load_all_mid
from utiles import get_update_docs
from utiles import write_row_by_doc
from utiles import write_row_by_mid


def main():
    url = sys.argv[1]
    if not url.startswith('https://space.bilibili.com'):
        print("[!!!] Not subscriable url, please check!")
        return
    mid = extract_mid(url)
    old_mids = load_all_mid()
    if mid not in old_mids:
        docs = get_update_docs(mid)
        write_row_by_mid(mid)
        for doc in docs:
            write_row_by_doc(doc, download=1)
        print("[---] Successfully added!")
    else:
        print("[---] Already added before!")


if __name__ == '__main__':
    main()
