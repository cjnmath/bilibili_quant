#! /usr/bin/python3

from utiles import extract_aid
from utiles import load_all_aid
from utiles import write_row_by_aid
import sys


def is_video_page_url(url):
    l = [
        'https://www.bilibili.com/video/av',
        'http://www.bilibili.com/video/av',
        'https://www.bilibili.com/bangumi/play',
        'http://www.bilibili.com/bangumi/play'
    ]
    for i in l:
        if url.startswith(i):
            return True
    return False


def main():
    record_aid = load_all_aid()
    url = sys.argv[1]
    if not is_video_page_url(url):
        print("That's not a bilibili video page link, please check.")
        return
    aid = extract_aid(url)
    if aid in record_aid:
        print("Already added before.")
        return
    write_row_by_aid(aid)
    print("Successfully added!")


if __name__ == '__main__':
    main()
